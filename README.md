# dotfiles

Dotfiles that I use on my Linux installations. Managed with
[dotdrop](https://github.com/deadc0de6/dotdrop)
